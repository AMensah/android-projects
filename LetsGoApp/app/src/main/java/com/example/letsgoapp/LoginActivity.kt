package com.example.letsgoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mAuthStateListener: FirebaseAuth.AuthStateListener
    private lateinit var emailId: EditText
    private lateinit var password: EditText
    private lateinit var btnSignIn: Button
    private lateinit var tvSignUp: TextView
    lateinit var currentid: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mFirebaseAuth = FirebaseAuth.getInstance()
        emailId = findViewById(R.id.LogEmail)
        password = findViewById(R.id.LogPassword)
        btnSignIn = findViewById(R.id.LogButton)
        tvSignUp = findViewById(R.id.LogtextView)

        mAuthStateListener = FirebaseAuth.AuthStateListener { auth ->
            val mFirebaseUser = auth.currentUser
            if(mFirebaseUser != null)
            {
                currentid = mFirebaseUser!!.uid.toString()
                Toast.makeText(this@LoginActivity,"You are logged in!!......"+currentid, Toast.LENGTH_SHORT).show()
                var i = Intent(this@LoginActivity,HomeActivity::class.java)
                i.putExtra("CurrentId",currentid)
                startActivity(i)

            }
            else
            {
                Toast.makeText(this@LoginActivity,"Please log in!", Toast.LENGTH_SHORT).show()
            }
        }

        btnSignIn.setOnClickListener {

            var email = emailId.text.toString()
            var pwd = password.text.toString()

            if (email.isEmpty())
            {
                emailId.setError("Please enter email")
                emailId.requestFocus()
            }
            else if (pwd.isEmpty())
            {
                password.setError("Please enter a password")
                password.requestFocus()
            }
            else if(email.isEmpty() && pwd.isEmpty())
            {
                Toast.makeText(this@LoginActivity,"Required fields are emplty!!!", Toast.LENGTH_SHORT).show()
            }
            else if(!(email.isEmpty() && pwd.isEmpty()))
            {
                this.mFirebaseAuth.signInWithEmailAndPassword(email,pwd).addOnCompleteListener(this){task ->

                    if ( !task.isSuccessful)
                    {
                        Toast.makeText(this@LoginActivity,"Login error, email or password is incorrect, please try again!!",
                            Toast.LENGTH_SHORT).show()
                    }
                    else
                    {
                        val intToProfile = Intent(this@LoginActivity,HomeActivity::class.java)
                        intToProfile.putExtra("CurrentId",currentid)
                        startActivity(intToProfile)
                    }
                }



            }
            else
            {
                Toast.makeText(this@LoginActivity,"Error occurred!!", Toast.LENGTH_SHORT).show()

            }
        }
        tvSignUp.setOnClickListener {
            val intSignup = Intent(this@LoginActivity,MainActivity::class.java)
            startActivity(intSignup)
        }

    }

    override fun onStart() {
        super.onStart()
        mFirebaseAuth.addAuthStateListener(mAuthStateListener)
    }
}
