package com.example.letsgoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_profile_acitivity.*

class ProfileActivity : AppCompatActivity() {

    lateinit var name: EditText
    lateinit var number: EditText
    lateinit var startdes: EditText
    lateinit var enddes: EditText
    lateinit var starttime: EditText
    lateinit var endtime: EditText
    lateinit var curid: String

    lateinit var update: Button

    private lateinit var mDbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_acitivity)

        var intent = intent

        curid = intent.getStringExtra("CurrentId")


        name = findViewById(R.id.editTextProfileName)
        number = findViewById(R.id.editTextProfilePhone)
        startdes = findViewById(R.id.editTextStartDes)
        enddes = findViewById(R.id.editTextEndDes)
        starttime = findViewById(R.id.editTextStime)
        endtime = findViewById(R.id.editTextEtime)

        update = findViewById(R.id.buttonProfileUpdate)

        update.setOnClickListener {
            update()
        }

    }

    private fun update(){

        val nm = name.text.toString().trim()
        if(nm.isEmpty()){
            editTextProfileName.error = "Please enter your Full Name"
            return
        }
        val num = number.text.toString().trim()
        if(num.isEmpty()){
            editTextProfilePhone.error = "Please enter your Phone Number"
            return
        }
        val sdes = startdes.text.toString().trim()
        if(sdes.isEmpty()){
            editTextStartDes.error = "Please enter a Strat Destination"
            return
        }
        val edes = enddes.text.toString().trim()
        if(edes.isEmpty()){
            editTextEndDes.error = "Please enter a End Destination"
            return
        }
        val stime = starttime.text.toString().trim()
        if(stime.isEmpty()){
            editTextStime.error = "Please enter a Strat Time"
            return
        }
        val etime = endtime.text.toString().trim()
        if(etime.isEmpty()){
            editTextEtime.error = "Please enter a End Time"
            return
        }

        //database initialization and connection

        val mDatabase = FirebaseDatabase.getInstance()
        mDbRef = mDatabase.getReference("People")

        //Setting firebase unique key for Hashmap list
        val userId = curid

        //creating person
        val curperson = Person(userId,nm,num,sdes,edes, stime, etime,4)
        mDbRef.child(userId).setValue(curperson)

        Toast.makeText(this@ProfileActivity,"Succesfully posted person",Toast.LENGTH_SHORT).show()

        val intoHome = Intent(this@ProfileActivity,HomeActivity::class.java)
        intoHome.putExtra("CurrentId",curid)
        startActivity(intoHome)

    }

}
