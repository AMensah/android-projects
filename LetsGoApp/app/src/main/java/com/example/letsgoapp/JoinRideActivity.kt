package com.example.letsgoapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class JoinRideActivity : AppCompatActivity() {

    lateinit var curid: String
    lateinit var curNumber: String
    lateinit var curname:String
    lateinit var curSDes:String
    lateinit var curStime:String
    lateinit var curEDes: String
    lateinit var curETime: String
    lateinit var curSeats: String
    lateinit var currentperson: TextView
    lateinit var profilepic: ImageView
    lateinit var joinStart: TextView
    lateinit var joinStime: TextView
    lateinit var joinend: TextView
    lateinit var joinETime: TextView
    lateinit var callcur: Button
    lateinit var joinperson: Button
    lateinit var joinseats: TextView

    private lateinit var mDbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_ride)


        var intent = intent

        curid = intent.getStringExtra("CurId")
        curname = intent.getStringExtra("CurName")
        curNumber = intent.getStringExtra("CurNumber").trim()
        curSDes = intent.getStringExtra("CurSDes")
        curEDes = intent.getStringExtra("CurEDes")
        curStime = intent.getStringExtra("CurSTime")
        curETime = intent.getStringExtra("CurETime")
        curSeats = intent.getStringExtra("CurSeats")

        currentperson = findViewById(R.id.textViewJoin)
        profilepic = findViewById(R.id.imageViewJoin)
        joinStart = findViewById(R.id.textViewJoinStart)
        joinStime = findViewById(R.id.textViewJoinStartTime)
        joinend = findViewById(R.id.textViewJoinEnd)
        joinETime = findViewById(R.id.textViewjoinEndTime)
        callcur = findViewById(R.id.buttonJoinContact)
        joinperson = findViewById(R.id.buttonJoinJoin)
        joinseats = findViewById(R.id.textViewJoinSeats)



        currentperson.text = curname
        joinStart.text = curSDes
        joinStime.text = curStime
        joinend.text = curEDes
        joinETime.text = curETime
        joinseats.text = curSeats


        callcur.setOnClickListener {
            //Dailer intent
            val cintent = Intent(Intent.ACTION_DIAL, Uri.parse("tel: " + Uri.encode(curNumber)))
            Toast.makeText(this@JoinRideActivity,"Calling: "+curNumber,Toast.LENGTH_SHORT).show()
            startActivity(cintent)
        }

        joinperson.setOnClickListener {

            val numberofSeats = curSeats.toInt() - 1

            //database initialization and connection

            val mDatabase = FirebaseDatabase.getInstance()
            mDbRef = mDatabase.getReference("People")

            val userId = curid

            //updating specific number to database
            mDbRef.child(userId).child("seats").setValue(numberofSeats)

            //success Message and redirection
            Toast.makeText(this@JoinRideActivity,"Successfully Joined",Toast.LENGTH_SHORT).show()

            val intoHome = Intent(this@JoinRideActivity,HomeActivity::class.java)
            intoHome.putExtra("CurrentId",curid)
            startActivity(intoHome)

        }


    }
}
