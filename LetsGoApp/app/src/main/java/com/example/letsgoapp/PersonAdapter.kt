package com.example.letsgoapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

class PersonAdapter(val mCtx: Context, val layoutResId: Int, val personList: List<Person>)
    : ArrayAdapter<Person>(mCtx,layoutResId,personList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId, null)

        val imageView:ImageView = view.findViewById(R.id.PersonsImage)
        val textViewName = view.findViewById<TextView>(R.id.textViewPersonsName)
        val textViewStartDes = view.findViewById<TextView>(R.id.textViewPersonsStart)
        val textViewEndDes = view.findViewById<TextView>(R.id.textPersonsEnd)
        val textViewStartTime = view.findViewById<TextView>(R.id.textViewPersonsStaTime)
        val textViewEndTime = view.findViewById<TextView>(R.id.textViewPersonsDesTime)
        val textViewSeats = view.findViewById<TextView>(R.id.textViewPersonsSeats)

        val person = personList[position]
        textViewName.text = person.name
        textViewStartDes.text = person.sdes
        textViewEndDes.text = person.edes
        textViewStartTime.text = person.stime
        textViewEndTime.text = person.etime
        textViewSeats.text = "  Seats: "+person.seats.toString()


        return view
    }
}