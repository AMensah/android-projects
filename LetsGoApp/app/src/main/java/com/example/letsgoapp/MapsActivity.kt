package com.example.letsgoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions



///////////////////////////////////////////
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.view.View
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import java.util.*
import com.google.android.gms.maps.model.Marker
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.layoutcustommapmarker.view.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOError
import kotlinx.android.synthetic.main.activity_join_ride.*



data class InfoWindowData(val mLocatioName: String,
                          val mLocationAddres: String)
class CustomInfoWindowGoogleMap(val context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(p0: Marker?): View {
        var mInfoView = (context as
                Activity).layoutInflater.inflate(R.layout.layoutcustommapmarker, null)
        var mInfoWindow: InfoWindowData? = p0?.tag as InfoWindowData?
        mInfoView.txtLocMarkerName.text = mInfoWindow?.mLocatioName
        mInfoView.imageView.contentDescription = mInfoWindow?.mLocationAddres
        return mInfoView
    }
    override fun getInfoWindow(p0: Marker?): View? {
        return null
    }
}
///////////////////////////////////////////////////////////

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var btnLogout: Button
    private lateinit var btnAddTrip: Button
    private lateinit var btnfilterPage: Button
    lateinit var curid: String

    lateinit var mDbRef: DatabaseReference
    lateinit var personList: MutableList<Person>
    lateinit var listView: ListView

    private lateinit var mMap: GoogleMap



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)






        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap




        personList = mutableListOf()
        val mDatabase = FirebaseDatabase.getInstance()
        mDbRef = mDatabase.getReference("People")




        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {


                if(p0!!.exists()){

                    for (p in p0.children){
                        val person = p.getValue(Person::class.java)
                        personList.add(person!!)

                        //////////////////////
                        var geocodemaches : List<Address>? = null
                        try {
                            geocodemaches = Geocoder(this@MapsActivity).getFromLocationName(person.sdes.toString(), 1)
                        }catch (e: IOError){
                        }

                        val apos = LatLng(geocodemaches!![0].latitude , geocodemaches!![0].longitude)
                        val mark = MarkerOptions().position(apos).title(person.name)
                        val info = InfoWindowData(person.sdes ,person.id)
                        val customInfoWindow = CustomInfoWindowGoogleMap(this@MapsActivity)
                        mMap.setInfoWindowAdapter( customInfoWindow)
                        val aa = mMap.addMarker(mark)
                        aa.tag = info
                        ///////////////////

                    }

                    val adapter = PersonAdapter(applicationContext, R.layout.persons, personList)

                    mMap.setOnMarkerClickListener { marker ->

                        val i = Intent(this@MapsActivity,JoinRideActivity::class.java)
                        i.putExtra("CurId",marker.title)
                        i.putExtra("CurSDes",marker.title)
                        i.putExtra("CurEDes","dghdh")
                        i.putExtra("CurSTime","sdgeg")
                        i.putExtra("CurETime","wegweg")
                        i.putExtra("CurNumber","wegseg")
                        i.putExtra("CurName","sdgsweg")
                        i.putExtra("CurSeats","segesg")
                        startActivity(i)
                        true
                    }



                    val po = LatLng(52.13,5.29)
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(po))
                    //mMap.zoom
                    mMap.setOnInfoWindowClickListener {
                        var i = Intent(this@MapsActivity,JoinRideActivity::class.java)

                    }
                }
            }

        })







    }
}
