package com.example.letsgoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private lateinit var btnLogout: Button
    private lateinit var btnAddTrip: Button
    private lateinit var btnfilterPage: Button
    lateinit var curid: String

    lateinit var mDbRef: DatabaseReference
    lateinit var personList: MutableList<Person>
    lateinit var listView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        var intent = intent

        curid = intent.getStringExtra("CurrentId")

        personList = mutableListOf()
        val mDatabase = FirebaseDatabase.getInstance()
        mDbRef = mDatabase.getReference("People")

        listView = findViewById(R.id.listView)

        btnAddTrip = findViewById(R.id.buttonEditprofile)

        btnAddTrip.setOnClickListener {
            var toprofile = Intent(this@HomeActivity,ProfileActivity::class.java)
            toprofile.putExtra("CurrentId",curid)
            startActivity(toprofile)
        }

        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {

                if(p0!!.exists()){

                    for (p in p0.children){
                        val person = p.getValue(Person::class.java)
                        personList.add(person!!)
                    }

                    val adapter = PersonAdapter(applicationContext, R.layout.persons, personList)
                    listView.adapter = adapter

                }
            }

        })

        //set event listener for clicking on list view
        listView.setOnItemClickListener{parent,view, position, id->
            val curP = parent.getItemAtPosition(position) as Person

            val curid = curP.id
            val curstart = curP.sdes
            val curend =curP.edes
            val curstime = curP.stime
            val curetime = curP.etime
            val curnum = curP.number
            val curname = curP.name
            val curseats = curP.seats.toString().trim()

            Toast.makeText(this@HomeActivity,curid,Toast.LENGTH_SHORT).show()
            var i = Intent(this@HomeActivity,JoinRideActivity::class.java)
            i.putExtra("CurId",curid)
            i.putExtra("CurSDes",curstart)
            i.putExtra("CurEDes",curend)
            i.putExtra("CurSTime",curstime)
            i.putExtra("CurETime",curetime)
            i.putExtra("CurNumber",curnum)
            i.putExtra("CurName",curname)
            i.putExtra("CurSeats",curseats)
            startActivity(i)

        }


        btnLogout = findViewById(R.id.HomeSignOut)

        btnLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            var intToMain = Intent(this@HomeActivity,MainActivity::class.java)
            startActivity(intToMain)
        }

        btnfilterPage = findViewById(R.id.buttonFilterPage)

        btnfilterPage.setOnClickListener {
            var intToFilterPage = Intent(this@HomeActivity,FilterActivity::class.java)
            intToFilterPage.putExtra("CurrentId",curid)
            startActivity(intToFilterPage)
        }

        BtnMaps.setOnClickListener {
            val a = Intent(this, MapsActivity::class.java)
            startActivity(a)
        }
    }
}
