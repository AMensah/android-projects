package com.example.letsgoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.activity_maps.*

class FilterActivity : AppCompatActivity() {

    lateinit var startDes : EditText
    lateinit var endDes: EditText
    lateinit var startTime: EditText
    lateinit var endTime: EditText
    lateinit var filterOptions: Button
    lateinit var toprofile: TextView

    lateinit var mDbRef: DatabaseReference
    lateinit var personList: MutableList<Person>
    lateinit var listView: ListView

    lateinit var curid: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        var intent = intent

        curid = intent.getStringExtra("CurrentId")

        startDes = findViewById(R.id.editTextFilterStart)
        endDes = findViewById(R.id.editTextFilterEnd)
        startTime = findViewById(R.id.editTextFilterStartTime)
        endTime = findViewById(R.id.editTextFilterEndTime)
        filterOptions = findViewById(R.id.ButtonFilter)


        filterOptions.setOnClickListener {

            options()
        }


        toprofile = findViewById(R.id.textViewtoProfile)
        toprofile.setOnClickListener {
            var toprofile = Intent(this@FilterActivity,ProfileActivity::class.java)
            toprofile.putExtra("CurrentId",curid)
            startActivity(toprofile)
        }

    }

    private fun options(){

        val sdes = startDes.text.toString().trim()
        if(sdes.isEmpty()){
            editTextFilterStart.error = "Please enter a Strat Destination"
            return
        }
        val edes = endDes.text.toString().trim()
        if(edes.isEmpty()){
            editTextFilterEnd.error = "Please enter a End Destination"
            return
        }
        val stim = startTime.text.toString().trim()
        if(stim.isEmpty()){
            editTextFilterStartTime.error = "Please enter a Strat Time"
            return
        }
        val etim = endTime.text.toString().trim()
        if(etim.isEmpty()){
            editTextFilterEndTime.error = "Please enter a End Time"
            return
        }

        personList = mutableListOf()
        val mDatabase = FirebaseDatabase.getInstance()
        mDbRef = mDatabase.getReference("People")

        listView = findViewById(R.id.listViewFilter)


        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {

                if(p0!!.exists()){

                    for (p in p0.children){
                        val person = p.getValue(Person::class.java)
                        personList.add(person!!)
                    }

                    var filterList = personList.filter { it.sdes.toLowerCase().contains(sdes.toLowerCase())||
                                                         it.edes.toLowerCase().contains(edes.toLowerCase())||
                                                         it.stime == stim || it.etime == etim}

                    if(filterList.isEmpty()){
                        Toast.makeText(this@FilterActivity,"Sorry no Carpools to match your Filter",Toast.LENGTH_SHORT).show()
                    }
                    val adapter = PersonAdapter(applicationContext, R.layout.persons, filterList)
                    listView.adapter = adapter

                }
            }

        })


    }
}
