package com.example.snapme

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var emailId: EditText
    private lateinit var password: EditText
    private lateinit var btnSignup: Button
    private lateinit var tvSignIn: TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        mFirebaseAuth = FirebaseAuth.getInstance()
        emailId = findViewById(R.id.RegEmail)
        password = findViewById(R.id.RegPassword)
        btnSignup = findViewById(R.id.RegButton)
        tvSignIn = findViewById(R.id.RegTextView)

        btnSignup.setOnClickListener {

            var email = emailId.text.toString()
            var pwd = password.text.toString()

            if (email.isEmpty())
            {
                emailId.setError("Please enter email")
                emailId.requestFocus()
            }
            else if (pwd.isEmpty())
            {
                password.setError("Please enter a password")
                password.requestFocus()
            }
            else if(email.isEmpty() && pwd.isEmpty())
            {
                Toast.makeText(this@MainActivity,"Required fields are emplty!!!",Toast.LENGTH_SHORT).show()
            }
            else if(!(email.isEmpty() && pwd.isEmpty()))
            {
                this.mFirebaseAuth.createUserWithEmailAndPassword(email,pwd).addOnCompleteListener {task: Task<AuthResult> ->
                    if(!(task.isSuccessful))
                    {
                        Toast.makeText(this@MainActivity,"Registration Unsuccessful, Please try again",Toast.LENGTH_SHORT).show()
                    }
                    else
                    {
                        Toast.makeText(this@MainActivity,"Registration Completed :)", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@MainActivity,HomeActivity::class.java))
                    }
                }
            }
            else
            {
                Toast.makeText(this@MainActivity,"Error occurred!!",Toast.LENGTH_SHORT).show()

            }
        }

        tvSignIn.setOnClickListener {
            val i = Intent(this@MainActivity,LoginActivity::class.java)
            startActivity(i)
        }
    }
}
