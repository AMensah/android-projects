package com.example.sportify;

import android.view.View;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);

    private LoginActivity lActivity = null;

    @Before
    public void setUp() throws Exception {
        lActivity = activityTestRule.getActivity();
    }

    @Test
    public void test(){
        View view = lActivity.findViewById(R.id.LogEmail);
        assertNotNull(view);
    }

    @After
    public void tearDown() throws Exception {
        lActivity = null;
    }
}