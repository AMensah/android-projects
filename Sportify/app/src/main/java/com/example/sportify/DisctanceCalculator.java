package com.example.sportify;

import com.google.android.gms.maps.model.LatLng;

public class DisctanceCalculator {
    // Return the distance between two points in meters.
    static double distance(LatLng a, LatLng b) {
        final double R = 6378.137;
        double dLat = b.latitude * Math.PI / 180 - a.latitude * Math.PI / 180;
        double dLon = b.longitude * Math.PI / 180 - a.longitude * Math.PI / 180;
        double q = Math.pow(Math.sin(dLat/2), 2) + Math.cos(a.latitude * Math.PI / 180) * Math.cos(b.latitude * Math.PI / 180) * Math.pow(Math.sin(dLon/2), 2);
        double w = R * 2 * Math.atan2(Math.sqrt(q), Math.sqrt(1-q));
        return w * 1000;
    }
}
