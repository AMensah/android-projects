package com.example.sportify;

import android.os.Parcel;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EventUnitTest {

    @Test
    public void eventShouldWorkProperly(){

        Parcel mockedParcel = mock(Parcel.class);

        when(mockedParcel.readDouble()).thenReturn(2.00);
        when(mockedParcel.readString()).thenReturn("string");

        Event SUT = new Event(mockedParcel);
        SUT.getLat();

        //verify(mockedParcel,times(2)).readDouble();
        verify(mockedParcel,times(4)).readString();
    }
}
