package com.example.sportify;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DisctanceCalbulatorUnitTest {

    @Test
    public void distanceCalculatorShouldWorkProperly(){

        //LatLng mockedLatLngA = new LatLng(1.00,2.00);
        //LatLng mockedLatLngB = new LatLng(2.00,1.00);

        LatLng mockedLatLngA = mock(LatLng.class);
        LatLng mockedLatLngB = mock(LatLng.class);

        when(mockedLatLngA.latitude).thenReturn(1.00);
        when(mockedLatLngA.longitude).thenReturn(2.00);

        when(mockedLatLngB.latitude).thenReturn(2.00);
        when(mockedLatLngB.longitude).thenReturn(1.00);

        DisctanceCalculator SUT = new DisctanceCalculator();
        SUT.distance(mockedLatLngA, mockedLatLngB);

        assertEquals( SUT.distance(mockedLatLngA, mockedLatLngB), SUT.distance(mockedLatLngA, mockedLatLngB), 0.01 );
        //assertEquals( 1.00, 1.00, 0.01 );

    }
}
