package com.example.sportify;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParticipantUnitTest {
    @Test
    public void participantShouldWorkProperly(){

        Participant SUT = new Participant("r@gmail.com", "12");

        SUT.setEventid("24");

        assertEquals( "24", SUT.getEventid() );
    }
}
