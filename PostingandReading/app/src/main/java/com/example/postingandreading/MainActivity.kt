package com.example.postingandreading

import android.app.Application
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var destination: EditText
    lateinit var time: EditText
    lateinit var buttonsave:Button
    lateinit var allp: TextView

    private lateinit var mDbRef: DatabaseReference
    private var userId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        destination = findViewById(R.id.editTextDestination)
        time = findViewById(R.id.editTextTime)
        buttonsave = findViewById(R.id.buttonSave)
        allp = findViewById(R.id.textViewAllPosts)

        buttonsave.setOnClickListener{
            savePost()
        }

        allp.setOnClickListener {
            val i = Intent(this@MainActivity,ViewActivity::class.java)
            startActivity(i)
        }

    }

    private fun savePost(){



        val des = destination.text.toString().trim()
            if(des.isEmpty()){
                editTextDestination.error = "Please enter a Destination"
                return
            }
        val tim = time.text.toString().trim()
            if(tim.isEmpty()){
                editTextTime.error = "Please input a time"
                return
            }

        val mDatabase = FirebaseDatabase.getInstance()
        mDbRef = mDatabase.getReference("Donor/Name")

//        val s = myRef.child("posts").push().key

        //Setting firebase unique key for Hashmap list
        val userId = mDbRef.push().key.toString()
// creating user object
//
        val user = Posts(userId,des,tim)
        mDbRef.child(userId).setValue(user);
//
//        myRef.child("posts").child(CurPost.uid).setValue(CurPost)

        Toast.makeText(this@MainActivity,"Posted",Toast.LENGTH_LONG).show()

        val i = Intent(this@MainActivity,ViewActivity::class.java)
        startActivity(i)
    }
}
