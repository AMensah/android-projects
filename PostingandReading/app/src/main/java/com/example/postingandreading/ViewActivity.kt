package com.example.postingandreading

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import com.google.firebase.database.*

class ViewActivity : AppCompatActivity() {
    private lateinit var mDbRef: DatabaseReference
    lateinit var postList: MutableList<Posts>
    lateinit var listView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view)


        postList = mutableListOf()
        val mDatabase = FirebaseDatabase.getInstance()
        mDbRef = mDatabase.getReference("Donor/Name")

        listView = findViewById(R.id.listView)

        mDbRef.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {

                if(p0!!.exists()){

                    for (p in p0.children){
                        val post = p.getValue(Posts::class.java)
                        postList.add(post!!)
                    }

                    val adapter = PostAdapter(applicationContext, R.layout.posts, postList)
                    listView.adapter = adapter

                }
            }

        })
    }
}
