package com.example.postingandreading

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class PostAdapter(val mCtx: Context,val layoutResId: Int,val postList: List<Posts>)
    :ArrayAdapter<Posts>(mCtx,layoutResId,postList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId, null)

        val textViewName = view.findViewById<TextView>(R.id.textViewName)

        val post = postList[position]
        textViewName.text = postList.indexOf(post).toString()+": Destination: "+post.des+" -Time: "+post.tim

        return view
    }
}